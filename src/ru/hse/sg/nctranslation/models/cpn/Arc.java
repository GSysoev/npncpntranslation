package ru.hse.sg.nctranslation.models.cpn;

/**
 * TODO: class description.
 * Date: 18.03.12
 * Time: 14:21
 *
 * @author German Sysoev (HSE)
 */
public class Arc {
    private Orientation orientation;
    private Place place;
    private Transition transition;
    private String annotation;

    public Arc(Place place, Transition transition,
               Orientation orientation, String annotation) {
        this.place = place;
        this.transition = transition;
        this.orientation = orientation;
        this.annotation = annotation;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public static enum Orientation {
        /**
         * From place to transition
         */
        PtoT,

        /**
         * From transition to place
         */
        TtoP;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Transition getTransition() {
        return transition;
    }

    public void setTransition(Transition transition) {
        this.transition = transition;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }
}
