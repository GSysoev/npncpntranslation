package ru.hse.sg.nctranslation.models.cpn;

import  ru.hse.sg.nctranslation.*;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * TODO: class description.
 * Date: 18.03.12
 * Time: 14:08
 *
 * @author German Sysoev (HSE)
 */
public class Cpn {
    private List<Place> places;
    private List<Transition> transitions;
    private List<Arc> arcs;
    private Set<String> vars;
    {
        places = new ArrayList<Place>();
        transitions = new ArrayList<Transition>();
        arcs = new ArrayList<Arc>();
        vars = new HashSet<String>();
        vars.add("x");
        vars.add("xr");
    }

    public Cpn() {

    }

    public List<Place> getPlaces() {
        return places;
    }

    public List<Transition> getTransitions() {
        return transitions;
    }

    public List<Arc> getArcs() {
        return arcs;
    }

    public Set<String> getVars() {
        return vars;
    }

    public String asXml() throws TransformerException, ParserConfigurationException, FileNotFoundException {
        return (new CpnXmlDoc(this)).asString();
    }


}
