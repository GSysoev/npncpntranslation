package ru.hse.sg.nctranslation.models.cpn;

import java.awt.*;

/**
 * TODO: class description.
 * Date: 18.03.12
 * Time: 14:10
 *
 * @author German Sysoev (HSE)
 */
public class Place {
    private String name;
    private String type;
    private String initMark;

    private String id; //this is for xml
    private Point coordinate; //this is for xml

    private static int placeNum = 0;

    public Place() {
        name = "P" + placeNum++;
    }

    public Place(String name) {
        this.name = name;
    }

     public Place(String type, String initMark) {
        name = "P" + placeNum++;
        this.type = type;
        this.initMark = initMark;
    }

    public Place(String name, String type, String initMark) {
        this.name = name;
        this.type = type;
        this.initMark = initMark;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInitMark() {
        return initMark;
    }

    public void setInitMark(String initMark) {
        this.initMark = initMark;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Point getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Point coordinate) {
        this.coordinate = coordinate;
    }
}
