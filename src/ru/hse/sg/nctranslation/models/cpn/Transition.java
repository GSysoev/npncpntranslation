package ru.hse.sg.nctranslation.models.cpn;

import java.awt.*;

/**
 * TODO: class description.
 * Date: 18.03.12
 * Time: 14:21
 *
 * @author German Sysoev (HSE)
 */
public class Transition {
    private String name = "";
    private String condition = "";
    private String code = "";

    private String id; //this is for xml
    private Point coordinate; //this is for xml

    private static int transNum = 0;


    public Transition() {
        name = "T" + transNum++;
    }

    public Transition(String name) {
        this.name = name;
    }

    public Transition(String condition, String code) {
        name = "T" + transNum++;
        this.condition = condition;
        this.code = code;
    }

    public Transition(String name, String condition, String code) {
        this.name = name;
        this.condition = condition;
        this.code = code;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Point getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Point coordinate) {
        this.coordinate = coordinate;
    }
}
