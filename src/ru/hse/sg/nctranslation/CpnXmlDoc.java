package ru.hse.sg.nctranslation;

import org.w3c.dom.*;
import ru.hse.sg.nctranslation.models.cpn.Arc;
import ru.hse.sg.nctranslation.models.cpn.Cpn;
import ru.hse.sg.nctranslation.models.cpn.Place;
import ru.hse.sg.nctranslation.models.cpn.Transition;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.awt.*;
import java.io.*;

/**
 * TODO: class description.
 * Date: 15.03.12
 * Time: 13:40
 *
 * @author German Sysoev (HSE)
 */
public class CpnXmlDoc {
    private  Cpn cpn;
    private Document doc;

    private int id = 99;
    private static final String PAGE_ID = "ID0";
    private static final String INSTANCE_ID = "ID1";

    /**
     * Constructs xml representation of the <tt>cpn</tt>
     *
     * @param cpn a cpn object to translate
     */
    public CpnXmlDoc(Cpn cpn) throws ParserConfigurationException, TransformerException {

        this.cpn = cpn;

        //We need a Document
        DocumentBuilderFactory dbFac = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbFac.newDocumentBuilder();
        doc = docBuilder.newDocument();

        //Creating the XML tree
        //create the root element and add it to the document
        Element root = doc.createElement("workspaceElements");
        doc.appendChild(root);

        //<generator>
        Element generator = doc.createElement("generator");
        generator.setAttribute("tool", "NPN-CPN Translator");
        generator.setAttribute("version", "1.0");
        generator.setAttribute("format", "6");

        root.appendChild(generator);

        //<cpnet>
        Element cpnet = doc.createElement("cpnet");
        root.appendChild(cpnet);
        createGlobbox(cpnet);
        createPage(cpnet);
        createInstances(cpnet);
        createOptions(cpnet);
        createBinders(cpnet);
        createMonitorblock(cpnet);
        createIndexNode(cpnet);
    }

    /**
     * Returns next possible id
     *
     * @return next possible id as a string
     */
    private String nextID() {
        return "ID" + ++id;
    }

    /**
     * Adds <tt>globbox</tt> to xml
     */
    private void createGlobbox(Element cpnet) {
        //  Smth like this:
        //
        //    <globbox>
        //      <block id="ID1412310166">
        //        <id>Standard priorities</id>
        //      </block>
        //      <block id="ID1">
        //        <id>Standard declarations</id>
        //        <color id="ID3">
        //          <id>INT</id>
        //          <int/>
        //        </color>
        //        <color id="ID4">
        //          <id>INT</id>
        //          <int/>
        //        </color>
        //      </block>
        //    </globbox>

        Element globbox = doc.createElement("globbox");
        cpnet.appendChild(globbox);

        Element block1 = doc.createElement("block");
        block1.setAttribute("id", nextID());
        globbox.appendChild(block1);
        Element id1 = doc.createElement("id");
        block1.appendChild(id1);
        Text sp = doc.createTextNode("Standard priorities");
        id1.appendChild(sp);

        Element block2 = doc.createElement("block");
        block2.setAttribute("id", nextID());
        globbox.appendChild(block2);
        Element id2 = doc.createElement("id");
        block2.appendChild(id2);
        Text sd = doc.createTextNode("Standard declarations");
        id2.appendChild(sd);

        Element color1 = doc.createElement("color");
        color1.setAttribute("id", nextID());
        block2.appendChild(color1);

        Element id3 = doc.createElement("id");
        color1.appendChild(id3);
        Text intText = doc.createTextNode("INT");
        id3.appendChild(intText);

        Element intEl = doc.createElement("int");
        color1.appendChild(intEl);

        Element color2 = doc.createElement("color");
        color2.setAttribute("id", nextID());
        block2.appendChild(color2);

        Element id44 = doc.createElement("id");
        color2.appendChild(id44);
        Text listIntText = doc.createTextNode("LIST_INT");
        id44.appendChild(listIntText);

        Element list = doc.createElement("list");
        color2.appendChild(list);
        Element id45 = doc.createElement("id");
        list.appendChild(id45);
        Text listType = doc.createTextNode("INT");
        id45.appendChild(listType);

        for (String valName : cpn.getVars()) {
            createVarOfListInt(valName, block2);
        }

        createFunc("fun ms_comp_el [ ] [ ] = 0 " +
                "| ms_comp_el ( x :: xs ) ( y :: ys ) = " +
                "let " +
                "val el_res = if ( x=y ) then 0 else if ( x<y ) then 1 else 2 ; " +
                "val tail_res = ms_comp_el xs ys ; " +
                "in case tail_res of " +
                "0 => ( case el_res of 0 => 0 | 1 => 1 | 2=>2 | 3=>3) " +
                "| 1 => ( case el_res of 0 => 1 | 1 => 1 | 2=>3 | 3=>3) " +
                "| 2 => ( case el_res of 0 => 2 | 1 => 3 | 2 =>2 | 3 => 3 ) " +
                "| 3 => 3 end;", block2);

        createFunc("fun ms_comp [ ] [ ] = 0 " +
                "| ms_comp ( x :: xs ) ( y :: ys ) = " +
                "if ( x<y ) then ~1 else ( if x>y then 1 else ms_comp xs ys ) " +
                "| ms_comp _ _ = ~2 " +
                ";", block2);

        createFunc("fun ms_add ( [ x ] , [ y ] ) = [ x + y ] " +
                "| ms_add ( ( x :: xs ) , ( y :: ys ) ) = x+y :: ms_add ( xs , ys ) " +
                "| ms_add ( _,_ ) = [ ] ;",
                block2);

        createFunc("fun ms_le x y = " +
                "let " +
                "val res = ms_comp x y " +
                "in " +
                "if ( res = 0 ) orelse ( res = ~1 ) then true else false " +
                "end ;", block2);

        createFunc("fun ms_le_el x y = " +
                "let val res_comp = ms_comp_el x y " +
                "in " +
                "(res_comp=0 orelse res_comp =1) " +
                "end ;", block2);
    }

    private void createFunc(String code, Element parent) {
        Element func = doc.createElement("ml");
        func.setAttribute("id", nextID());
        func.appendChild(doc.createTextNode(code));

        Element lo = doc.createElement("layout");
        lo.appendChild(doc.createTextNode(code));
        func.appendChild(lo);

        parent.appendChild(func);
    }

    private void createVarOfListInt(String valName, Element parent) {
        Element var = doc.createElement("var");
        var.setAttribute("id", nextID());
        parent.appendChild(var);

        Element type = doc.createElement("type");
        Element id4 = doc.createElement("id");
        Text typeText = doc.createTextNode("LIST_INT");
        id4.appendChild(typeText);
        type.appendChild(id4);
        var.appendChild(type);

        Element id5 = doc.createElement("id");
        Text id5Text = doc.createTextNode(valName);
        id5.appendChild(id5Text);
        var.appendChild(id5);

        Element layout = doc.createElement("layout");
        Text layoutText = doc.createTextNode("var " + valName + " : LIST_INT;");
        layout.appendChild(layoutText);
        var.appendChild(layout);
    }
    /**
     * Adds <tt>page</tt> to xml
     */
    private void createPage(Element cpnet) {
        Element page = doc.createElement("page");
        page.setAttribute("id", PAGE_ID);
        cpnet.appendChild(page);

        Element pageattr = doc.createElement("pageattr");
        pageattr.setAttribute("name", "NpnAsCpn");
        page.appendChild(pageattr);

        for (Place place : cpn.getPlaces()) {
            createPlace(place, page);
        }

        for (Transition transition : cpn.getTransitions()) {
            createTransition(transition, page);
        }

        for (Arc arc : cpn.getArcs()) {
            createArc(arc, page);
        }
    }

    private final static int placeHeight = 40;
    private final static int placeWidth = 60;
    private int xPlace = - 430;
    private int yPlace = 230;

    private void createPlace(Place place, Element parent) {
        xPlace += 100;
        //yPlace -= 100;

        Element placeTag = doc.createElement("place");
        placeTag.setAttribute("id", nextID());
        place.setId(placeTag.getAttribute("id"));
        parent.appendChild(placeTag);

        Element posattr = doc.createElement("posattr");
        posattr.setAttribute("x", xPlace + "");
        posattr.setAttribute("y", yPlace + "");
        place.setCoordinate(new Point(xPlace, yPlace));
        placeTag.appendChild(posattr);

        Element fillattr = doc.createElement("fillattr");
        fillattr.setAttribute("colour", "White");
        fillattr.setAttribute("pattern", "");
        fillattr.setAttribute("filled", "false");
        placeTag.appendChild(fillattr);

        Element lineattr = doc.createElement("lineattr");
        lineattr.setAttribute("colour", "Black");
        lineattr.setAttribute("thick", "1");
        lineattr.setAttribute("type", "Solid");
        placeTag.appendChild(lineattr);

        Element textattr = doc.createElement("textattr");
        textattr.setAttribute("colour", "Black");
        textattr.setAttribute("bold", "false");
        placeTag.appendChild(textattr);

        Element text = doc.createElement("text");
        Text name = doc.createTextNode(place.getName());
        text.appendChild(name);
        placeTag.appendChild(text);

        Element ellipse = doc.createElement("ellipse");
        ellipse.setAttribute("w", placeWidth + "");
        ellipse.setAttribute("h", placeHeight + "");
        placeTag.appendChild(ellipse);

        Element token = doc.createElement("token");
        token.setAttribute("x", "-10");
        token.setAttribute("y", "0");
        placeTag.appendChild(token);

        Element marking = doc.createElement("marking");
        marking.setAttribute("x", "0");
        marking.setAttribute("y", "0");
        marking.setAttribute("hidden", "false");
        Element snap = doc.createElement("snap");
        snap.setAttribute("snap_id", "0");
        snap.setAttribute("anchor.horizontal", "0");
        snap.setAttribute("anchor.vertical", "0");
        marking.appendChild(snap);
        placeTag.appendChild(marking);

        createPlaceType(placeTag, place);

        createPlaceInitmark(placeTag, place);
    }

    private void createPlaceType(Element placeTag, Place place) {
        Element type = doc.createElement("type");
        type.setAttribute("id", nextID());
        placeTag.appendChild(type);

        Element posattr = doc.createElement("posattr");
        posattr.setAttribute("x", (xPlace + placeWidth / 2) + "");
        posattr.setAttribute("y", (yPlace - placeHeight / 2) + "");
        type.appendChild(posattr);

        Element fillattr = doc.createElement("fillattr");
        fillattr.setAttribute("colour", "White");
        fillattr.setAttribute("pattern", "Solid");
        fillattr.setAttribute("filled", "false");
        type.appendChild(fillattr);

        Element lineattr = doc.createElement("lineattr");
        lineattr.setAttribute("colour", "Black");
        lineattr.setAttribute("thick", "0");
        lineattr.setAttribute("type", "Solid");
        type.appendChild(lineattr);

        Element textattr = doc.createElement("textattr");
        textattr.setAttribute("colour", "Black");
        textattr.setAttribute("bold", "false");
        type.appendChild(textattr);

        Element text = doc.createElement("text");
        text.setAttribute("tool", "CPN Tools");
        text.setAttribute("version", "3.2.2");
        Text name = doc.createTextNode(place.getType());
        text.appendChild(name);
        type.appendChild(text);
    }

    private void createPlaceInitmark(Element placeTag, Place place) {
        Element initmark = doc.createElement("initmark");
        initmark.setAttribute("id", nextID());
        placeTag.appendChild(initmark);

        Element posattr = doc.createElement("posattr");
        posattr.setAttribute("x", (xPlace + placeWidth / 2) + "");
        posattr.setAttribute("y", (yPlace + placeHeight / 2) + "");
        initmark.appendChild(posattr);

        Element fillattr = doc.createElement("fillattr");
        fillattr.setAttribute("colour", "White");
        fillattr.setAttribute("pattern", "Solid");
        fillattr.setAttribute("filled", "false");
        initmark.appendChild(fillattr);

        Element lineattr = doc.createElement("lineattr");
        lineattr.setAttribute("colour", "Black");
        lineattr.setAttribute("thick", "0");
        lineattr.setAttribute("type", "Solid");
        initmark.appendChild(lineattr);

        Element textattr = doc.createElement("textattr");
        textattr.setAttribute("colour", "Black");
        textattr.setAttribute("bold", "false");
        initmark.appendChild(textattr);

        Element text = doc.createElement("text");
        text.setAttribute("tool", "CPN Tools");
        text.setAttribute("version", "3.2.2");
        Text name = doc.createTextNode(place.getInitMark());
        text.appendChild(name);
        initmark.appendChild(text);
    }

    private final static int transHeight = 40;
    private final static int transWidth = 60;
    private int xTrans = - 300;
    private int yTrans = 130;

    private void createTransition(Transition trans, Element parent) {
        xTrans += 100;
        //yTrans -= 100;

        Element transTag = doc.createElement("trans");
        transTag.setAttribute("id", nextID());
        trans.setId(transTag.getAttribute("id"));
        transTag.setAttribute("explicit", "false");
        parent.appendChild(transTag);

        Element posattr = doc.createElement("posattr");
        posattr.setAttribute("x", xTrans + "");
        posattr.setAttribute("y", yTrans + "");
        trans.setCoordinate(new Point(xTrans, yTrans));
        transTag.appendChild(posattr);

        Element fillattr = doc.createElement("fillattr");
        fillattr.setAttribute("colour", "White");
        fillattr.setAttribute("pattern", "");
        fillattr.setAttribute("filled", "false");
        transTag.appendChild(fillattr);

        Element lineattr = doc.createElement("lineattr");
        lineattr.setAttribute("colour", "Black");
        lineattr.setAttribute("thick", "1");
        lineattr.setAttribute("type", "Solid");
        transTag.appendChild(lineattr);

        Element textattr = doc.createElement("textattr");
        textattr.setAttribute("colour", "Black");
        textattr.setAttribute("bold", "false");
        transTag.appendChild(textattr);

        Element text = doc.createElement("text");
        Text name = doc.createTextNode(trans.getName());
        text.appendChild(name);
        transTag.appendChild(text);

        Element box = doc.createElement("box");
        box.setAttribute("w", transWidth + "");
        box.setAttribute("h", transHeight + "");
        transTag.appendChild(box);

        Element binding = doc.createElement("binding");
        binding.setAttribute("x", "0");
        binding.setAttribute("y", "0");

        createTransCond(transTag, trans);

        createTransTime(transTag, trans);

        createTransCode(transTag, trans);

        createTransChannel(transTag, trans);

        createTransPriority(transTag, trans);
    }

    private void createTransCond(Element transTag, Transition trans) {
        Element cond = doc.createElement("cond");
        cond.setAttribute("id", nextID());
        transTag.appendChild(cond);

        Element posattr = doc.createElement("posattr");
        posattr.setAttribute("x", (xTrans - transWidth / 6) + "");
        posattr.setAttribute("y", (yTrans + transHeight / 1.4) + "");
        cond.appendChild(posattr);

        Element fillattr = doc.createElement("fillattr");
        fillattr.setAttribute("colour", "White");
        fillattr.setAttribute("pattern", "Solid");
        fillattr.setAttribute("filled", "false");
        cond.appendChild(fillattr);

        Element lineattr = doc.createElement("lineattr");
        lineattr.setAttribute("colour", "Black");
        lineattr.setAttribute("thick", "0");
        lineattr.setAttribute("type", "Solid");
        cond.appendChild(lineattr);

        Element textattr = doc.createElement("textattr");
        textattr.setAttribute("colour", "Black");
        textattr.setAttribute("bold", "false");
        cond.appendChild(textattr);

        Element text = doc.createElement("text");
        text.setAttribute("tool", "CPN Tools");
        text.setAttribute("version", "3.2.2");
        Text condText = doc.createTextNode(trans.getCondition());
        text.appendChild(condText);
        cond.appendChild(text);
    }

    private void createTransTime(Element transTag, Transition trans) {
        Element time = doc.createElement("time");
        time.setAttribute("id", nextID());
        transTag.appendChild(time);

        Element posattr = doc.createElement("posattr");
        posattr.setAttribute("x", (xTrans + transWidth / 2) + "");
        posattr.setAttribute("y", (yTrans + transHeight / 2) + "");
        time.appendChild(posattr);

        Element fillattr = doc.createElement("fillattr");
        fillattr.setAttribute("colour", "White");
        fillattr.setAttribute("pattern", "Solid");
        fillattr.setAttribute("filled", "false");
        time.appendChild(fillattr);

        Element lineattr = doc.createElement("lineattr");
        lineattr.setAttribute("colour", "Black");
        lineattr.setAttribute("thick", "0");
        lineattr.setAttribute("type", "Solid");
        time.appendChild(lineattr);

        Element textattr = doc.createElement("textattr");
        textattr.setAttribute("colour", "Black");
        textattr.setAttribute("bold", "false");
        time.appendChild(textattr);

        Element text = doc.createElement("text");
        text.setAttribute("tool", "CPN Tools");
        text.setAttribute("version", "3.2.2");
        //Text can be here
        time.appendChild(text);
    }

    private void createTransCode(Element transTag, Transition trans) {
        Element code = doc.createElement("code");
        code.setAttribute("id", nextID());
        transTag.appendChild(code);

        Element posattr = doc.createElement("posattr");
        posattr.setAttribute("x", (xTrans + transWidth / 1.5) + "");
        posattr.setAttribute("y", (yTrans - transHeight / 0.5) + "");
        code.appendChild(posattr);

        Element fillattr = doc.createElement("fillattr");
        fillattr.setAttribute("colour", "White");
        fillattr.setAttribute("pattern", "Solid");
        fillattr.setAttribute("filled", "false");
        code.appendChild(fillattr);

        Element lineattr = doc.createElement("lineattr");
        lineattr.setAttribute("colour", "Black");
        lineattr.setAttribute("thick", "0");
        lineattr.setAttribute("type", "Solid");
        code.appendChild(lineattr);

        Element textattr = doc.createElement("textattr");
        textattr.setAttribute("colour", "Black");
        textattr.setAttribute("bold", "false");
        code.appendChild(textattr);

        Element text = doc.createElement("text");
        text.setAttribute("tool", "CPN Tools");
        text.setAttribute("version", "3.2.2");
        Text codeText = doc.createTextNode(trans.getCode());
        text.appendChild(codeText);
        code.appendChild(text);
    }

    private void createTransChannel(Element transTag, Transition trans) {
        Element channel = doc.createElement("channel");
        channel.setAttribute("id", nextID());
        transTag.appendChild(channel);

        Element posattr = doc.createElement("posattr");
        posattr.setAttribute("x", (xTrans + transWidth / 2) + "");
        posattr.setAttribute("y", (yTrans) + "");
        channel.appendChild(posattr);

        Element fillattr = doc.createElement("fillattr");
        fillattr.setAttribute("colour", "White");
        fillattr.setAttribute("pattern", "Solid");
        fillattr.setAttribute("filled", "false");
        channel.appendChild(fillattr);

        Element lineattr = doc.createElement("lineattr");
        lineattr.setAttribute("colour", "Black");
        lineattr.setAttribute("thick", "0");
        lineattr.setAttribute("type", "Solid");
        channel.appendChild(lineattr);

        Element textattr = doc.createElement("textattr");
        textattr.setAttribute("colour", "Black");
        textattr.setAttribute("bold", "false");
        channel.appendChild(textattr);

        Element text = doc.createElement("text");
        text.setAttribute("tool", "CPN Tools");
        text.setAttribute("version", "3.2.2");
        //Text can be here
        channel.appendChild(text);
    }

    private void createTransPriority(Element transTag, Transition trans) {
        Element priority = doc.createElement("priority");
        priority.setAttribute("id", nextID());
        transTag.appendChild(priority);

        Element posattr = doc.createElement("posattr");
        posattr.setAttribute("x", (xTrans - transWidth / 2) + "");
        posattr.setAttribute("y", (yTrans - transHeight / 2) + "");
        priority.appendChild(posattr);

        Element fillattr = doc.createElement("fillattr");
        fillattr.setAttribute("colour", "White");
        fillattr.setAttribute("pattern", "Solid");
        fillattr.setAttribute("filled", "false");
        priority.appendChild(fillattr);

        Element lineattr = doc.createElement("lineattr");
        lineattr.setAttribute("colour", "Black");
        lineattr.setAttribute("thick", "0");
        lineattr.setAttribute("type", "Solid");
        priority.appendChild(lineattr);

        Element textattr = doc.createElement("textattr");
        textattr.setAttribute("colour", "Black");
        textattr.setAttribute("bold", "false");
        priority.appendChild(textattr);

        Element text = doc.createElement("text");
        text.setAttribute("tool", "CPN Tools");
        text.setAttribute("version", "3.2.2");
        //Text can be here
        priority.appendChild(text);
    }

    private void createArc(Arc arc, Element parent) {
        Element arcTag = doc.createElement("arc");
        arcTag.setAttribute("id", nextID());
        arcTag.setAttribute("orientation", arc.getOrientation().name());
        arcTag.setAttribute("order", "1");
        parent.appendChild(arcTag);

        Element posattr = doc.createElement("posattr");
        posattr.setAttribute("x", "0");
        posattr.setAttribute("y", "0");
        arcTag.appendChild(posattr);

        Element fillattr = doc.createElement("fillattr");
        fillattr.setAttribute("colour", "White");
        fillattr.setAttribute("pattern", "");
        fillattr.setAttribute("filled", "false");
        arcTag.appendChild(fillattr);

        Element lineattr = doc.createElement("lineattr");
        lineattr.setAttribute("colour", "Black");
        lineattr.setAttribute("thick", "1");
        lineattr.setAttribute("type", "Solid");
        arcTag.appendChild(lineattr);

        Element textattr = doc.createElement("textattr");
        textattr.setAttribute("colour", "Black");
        textattr.setAttribute("bold", "false");
        arcTag.appendChild(textattr);

        Element arrowattr = doc.createElement("arrowattr");
        arrowattr.setAttribute("headsize", "1.2");
        arrowattr.setAttribute("currentcyckle", "2");
        arcTag.appendChild(arrowattr);

        Element transend = doc.createElement("transend");
        transend.setAttribute("idref", arc.getTransition().getId());
        arcTag.appendChild(transend);

        Element placeend = doc.createElement("placeend");
        placeend.setAttribute("idref", arc.getPlace().getId());
        arcTag.appendChild(placeend);

        createAnnot(arcTag, arc);

    }

    private void createAnnot(Element arcTag, Arc arc) {
        Element annot = doc.createElement("annot");
        annot.setAttribute("id", nextID());
        arcTag.appendChild(annot);

        Element posattr = doc.createElement("posattr");
        int xp = arc.getPlace().getCoordinate().x;
        int yp = arc.getPlace().getCoordinate().y;
        int xt = arc.getTransition().getCoordinate().x;
        int yt = arc.getTransition().getCoordinate().y;
        posattr.setAttribute("x", "" + (double)(xp + xt) / 2);
        posattr.setAttribute("y", "" + (double)(yp + yt + 16) / 2);
        annot.appendChild(posattr);

        Element fillattr = doc.createElement("fillattr");
        fillattr.setAttribute("colour", "White");
        fillattr.setAttribute("pattern", "Solid");
        fillattr.setAttribute("filled", "false");
        annot.appendChild(fillattr);

        Element lineattr = doc.createElement("lineattr");
        lineattr.setAttribute("colour", "Black");
        lineattr.setAttribute("thick", "0");
        lineattr.setAttribute("type", "Solid");
        annot.appendChild(lineattr);

        Element textattr = doc.createElement("textattr");
        textattr.setAttribute("colour", "Black");
        textattr.setAttribute("bold", "false");
        annot.appendChild(textattr);

        Element text = doc.createElement("text");
        text.setAttribute("tool", "CPN Tools");
        text.setAttribute("version", "3.2.2");
        Text str = doc.createTextNode(arc.getAnnotation());
        text.appendChild(str);
        annot.appendChild(text);

    }

    /**
     * Adds <tt>instances</tt> to xml
     */
    private void createInstances(Element cpnet) {
        //    <instances>
        //      <instance id="ID2149"
        //                page="ID6"/>
        //    </instances>

        Element instances = doc.createElement("instances");
        cpnet.appendChild(instances);

        Element instance = doc.createElement("instance");
        instance.setAttribute("id", INSTANCE_ID);
        instance.setAttribute("page", PAGE_ID);
        instances.appendChild(instance);
    }

    /**
     * Adds <tt>options</tt> to xml
     */
    private void createOptions(Element cpnet) {
        //    <options>
        //      <option name="realtimestamp">
        //        <value>
        //          <boolean>false</boolean>
        //        </value>
        //      </option>
        //      <option name="fair_be">
        //        <value>
        //          <boolean>false</boolean>
        //        </value>
        //      </option>
        //
        //      ..............
        //
        //     </options>
        Element options = doc.createElement("options");
        cpnet.appendChild(options);
        createOption(options, "realtimestamp", false);
        createOption(options, "fair_be", false);
        createOption(options, "global_fairness", false);
        createOption(options, "outputdirectory", "&lt;same as models&gt;");
        createOption(options, "repavg", true);
        createOption(options, "repciavg", true);
        createOption(options, "repcount", false);
        createOption(options, "repfistival", false);
        createOption(options, "replastval", false);
        createOption(options, "repmax", true);
        createOption(options, "repmin", true);
        createOption(options, "repssquare", false);
        createOption(options, "repssqdev", false);
        createOption(options, "repstddev", true);
        createOption(options, "repsum", false);
        createOption(options, "repvariance", false);
        createOption(options, "avg", true);
        createOption(options, "ciavg", false);
        createOption(options, "count", true);
        createOption(options, "firstval", false);
        createOption(options, "lastval", false);
        createOption(options, "max", true);
        createOption(options, "min", true);
        createOption(options, "ssquare", false);
        createOption(options, "ssqdev", false);
        createOption(options, "stddev", false);
        createOption(options, "sum", false);
        createOption(options, "variance", false);
        createOption(options, "firstupdate", false);
        createOption(options, "interval", false);
        createOption(options, "lastupdate", false);
        createOption(options, "untimedavg", true);
        createOption(options, "untimedciavg", false);
        createOption(options, "untimedcount", true);
        createOption(options, "untimedfirstval", false);
        createOption(options, "untimedlastval", false);
        createOption(options, "untimedmax", true);
        createOption(options, "untimedmin", true);
        createOption(options, "untimedssquare", false);
        createOption(options, "untimedssqdev", false);
        createOption(options, "untimedstddev", false);
        createOption(options, "untimedsum", true);
        createOption(options, "untimedvariance", false);
    }

    /**
     * Create <tt>option</tt> with attributes name="<tt>name</tt>"
     * and boolean="<tt>boll</tt>" as a child of <tt>parent</tt> element.
     */
    private void createOption(Element parent, String name, boolean bool) {
        Element option = doc.createElement("option");
        option.setAttribute("name", name);
        parent.appendChild(option);

        Element value = doc.createElement("value");
        option.appendChild(value);

        Element boolVal = doc.createElement("boolean");
        value.appendChild(boolVal);
        Text text = doc.createTextNode(bool ? "true" : "false");
        boolVal.appendChild(text);
    }

    /**
     * Create <tt>option</tt> with attributes name="<tt>name</tt>"
     * and text="<tt>str</tt>" as a child of <tt>parent</tt> element.
     */
    private void createOption(Element parent, String name, String str) {
        Element option = doc.createElement("option");
        option.setAttribute("name", name);
        parent.appendChild(option);

        Element value = doc.createElement("value");
        option.appendChild(value);

        Element textVal = doc.createElement("text");
        value.appendChild(textVal);
        Text text = doc.createTextNode(str);
        textVal.appendChild(text);
    }

    /**
     * Adds <tt>binders</tt> to xml
     */
    private void createBinders(Element cpnet) {

        //    <binders>
        //      <cpnbinder id="ID1412315954"
        //                 x="200"
        //                 y="50"
        //                 width="1000"
        //                 height="500">
        //        <sheets>
        //          <cpnsheet id="ID1412315766"
        //                    panx="0.000000"
        //                    pany="0.000000"
        //                    zoom="1.000000"
        //                    instance="ID2149">
        //            <zorder>
        //              <position value="0"/>
        //            </zorder>
        //          </cpnsheet>
        //        </sheets>
        //        <zorder>
        //          <position value="0"/>
        //        </zorder>
        //      </cpnbinder>
        //    </binders>
        Element binders = doc.createElement("binders");
        cpnet.appendChild(binders);

        Element cpnbinder = doc.createElement("cpnbinder");
        cpnbinder.setAttribute("id", nextID());
        cpnbinder.setAttribute("x", "200");
        cpnbinder.setAttribute("y", "50");
        cpnbinder.setAttribute("width", "1000");
        cpnbinder.setAttribute("height", "500");
        binders.appendChild(cpnbinder);

        Element sheets = doc.createElement("sheets");
        cpnbinder.appendChild(sheets);

        Element cpnsheet = doc.createElement("cpnsheet");
        cpnsheet.setAttribute("id", nextID());
        cpnsheet.setAttribute("panx", "0.000000");
        cpnsheet.setAttribute("pany", "0.000000");
        cpnsheet.setAttribute("zoom", "1.000000");
        cpnsheet.setAttribute("instance", INSTANCE_ID);
        sheets.appendChild(cpnsheet);

        Element zorder1 = doc.createElement("zorder");
        cpnsheet.appendChild(zorder1);

        Element position = doc.createElement("position");
        position.setAttribute("value", "0");
        zorder1.appendChild(position);

        Element zorder2 = doc.createElement("zorder");
        cpnbinder.appendChild(zorder2);

        Element position2 = doc.createElement("position");
        position2.setAttribute("value", "0");
        zorder2.appendChild(position2);
    }

    /**
     * Adds <tt>monitorblock</tt> to xml
     */
    private void createMonitorblock(Element cpnet) {
        //    <monitorblock name="Monitors"/>
        Element monitorblock = doc.createElement("monitorblock");
        monitorblock.setAttribute("name", "Monitors");
        cpnet.appendChild(monitorblock);
    }

    /**
     * Adds <tt>IndexNode</tt> to xml
     */
    private void createIndexNode(Element cpnet) {
        //    <IndexNode expanded="true"/>
        Element indexNode = doc.createElement("IndexNode");
        indexNode.setAttribute("expanded", "true");
        cpnet.appendChild(indexNode);
    }

    /**
     * Returns string represantation of the xml
     *
     * @return xml document as string
     */
    public String asString() throws TransformerException, FileNotFoundException {
        //set up a transformer
        TransformerFactory transfac = TransformerFactory.newInstance();
        Transformer trans = transfac.newTransformer();
        doc.setXmlStandalone(true);
        trans.setOutputProperty(OutputKeys.INDENT, "yes");
        trans.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "-//CPN//DTD CPNXML 1.0//EN");
        trans.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "http://cpntools.org/DTD/6/cpn.dtd");
        trans.setOutputProperty(OutputKeys.ENCODING, "iso-8859-1");

        //create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        trans.transform(source, result);
        String xmlString = sw.toString();

        return xmlString;
    }
}
