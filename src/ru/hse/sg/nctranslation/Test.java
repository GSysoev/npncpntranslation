package ru.hse.sg.nctranslation;

import ru.hse.pavel.model.*;
import ru.hse.pavel.model.ArcPtT;
import ru.hse.pavel.model.ArcTtP;
import ru.hse.pavel.model.Place;
import ru.hse.pavel.model.Transition;
import ru.hse.pavel.model.pn.*;
import ru.hse.sg.nctranslation.models.cpn.Cpn;
//import ru.hse.sg.nctranslation.models.cpn.*;
import javax.swing.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO: class description.
 * Date: 16.03.12
 * Time: 14:52
 *
 * @author German Sysoev (HSE)
 */
public class Test {
    public static void main(String[] args) throws TransformerException, ParserConfigurationException,
            FileNotFoundException {
        NestedPN npn = new NestedPN();
        Type en1 = new Type("EN1", true);
        Place p0 = new Place(en1);
        Place p1 = new Place(en1);
        Place p2 = new Place(en1);
        Transition t1 = new Transition();
        Arc a1 = new ArcPtT(p0, t1);
        Arc a2 = new ArcPtT(p1, t1);
        Arc a3 = new ArcTtP(t1, p2);
        npn.getTypes().add(en1);
        npn.getPlaces().add(p0);
        npn.getPlaces().add(p1);
        npn.getPlaces().add(p2);
        npn.getTrans().add(t1);
        npn.getArcs().add(a1);
        npn.getArcs().add(a2);
        npn.getArcs().add(a3);
        try {
            a1.setRule("x");
            a2.setRule("y");
            a3.setRule("x+y");
        } catch (RuleViolationException e) {
            e.printStackTrace();
        }

        PetriNet pnEN1 = new PetriNet();
        ru.hse.pavel.model.pn.Place p1_ = new ru.hse.pavel.model.pn.Place();
        ru.hse.pavel.model.pn.Place p2_ = new ru.hse.pavel.model.pn.Place();
        ru.hse.pavel.model.pn.Place p3_ = new ru.hse.pavel.model.pn.Place();
        ru.hse.pavel.model.pn.Place p4_ = new ru.hse.pavel.model.pn.Place();
        en1.setInitialMarking(new int[]{3, 0, 0, 2});
        ru.hse.pavel.model.pn.Transition t1_ = new ru.hse.pavel.model.pn.Transition();
        ru.hse.pavel.model.pn.Transition t2_ = new ru.hse.pavel.model.pn.Transition();
        ru.hse.pavel.model.pn.ArcPtT a1_ = new ru.hse.pavel.model.pn.ArcPtT(p1_, t1_);
        ru.hse.pavel.model.pn.ArcPtT a2_ = new ru.hse.pavel.model.pn.ArcPtT(p1_, t2_);
        ru.hse.pavel.model.pn.ArcPtT a3_ = new ru.hse.pavel.model.pn.ArcPtT(p3_, t2_);
        ru.hse.pavel.model.pn.ArcPtT a4_ = new ru.hse.pavel.model.pn.ArcPtT(p4_, t2_);
        ru.hse.pavel.model.pn.ArcTtP a5_ = new ru.hse.pavel.model.pn.ArcTtP(p2_, t1_);
        ru.hse.pavel.model.pn.ArcTtP a6_ = new ru.hse.pavel.model.pn.ArcTtP(p3_, t1_);
        ru.hse.pavel.model.pn.ArcTtP a7_ = new ru.hse.pavel.model.pn.ArcTtP(p2_, t2_);
        ru.hse.pavel.model.pn.ArcTtP a8_ = new ru.hse.pavel.model.pn.ArcTtP(p4_, t2_);
        pnEN1.addPlace(p1_);
        pnEN1.addPlace(p2_);
        pnEN1.addPlace(p3_);
        pnEN1.addPlace(p4_);
        pnEN1.addTransition(t1_);
        pnEN1.addTransition(t2_);

        List<ru.hse.pavel.model.pn.ArcPtT> arcIn1 = new ArrayList<ru.hse.pavel.model.pn.ArcPtT>();
        arcIn1.add(a1_);
        List<ru.hse.pavel.model.pn.ArcPtT> arcIn2 = new ArrayList<ru.hse.pavel.model.pn.ArcPtT>();
        arcIn2.add(a2_);
        arcIn2.add(a3_);
        arcIn2.add(a4_);
        List<ru.hse.pavel.model.pn.ArcTtP> arcOut1 = new ArrayList<ru.hse.pavel.model.pn.ArcTtP>();
        arcOut1.add(a5_);
        arcOut1.add(a6_);
        List<ru.hse.pavel.model.pn.ArcTtP> arcOut2 = new ArrayList<ru.hse.pavel.model.pn.ArcTtP>();
        arcOut2.add(a7_);
        arcOut2.add(a8_);
        t1_.setArcsIn(arcIn1);
        t2_.setArcsIn(arcIn2);
        t1_.setArcsOut(arcOut1);
        t2_.setArcsOut(arcOut2);
        en1.setPetriNet(pnEN1);
        pnEN1.loadMarking(en1.getInitialMarking());
        Token token1 = new Token(en1);
        Token token2 = new Token(en1);
        p0.addToken(token1);
        p1.addToken(token2);


        Translator translator = new Translator(npn);
        translator.translate();
        Cpn cpn = translator.getCpn();
        cpn.asXml();
        PrintWriter pw = new PrintWriter(new File("step4.cpn"));
        pw.append(cpn.asXml());
        pw.close();



//        NestedPN npn = new NestedPN();
//        Type intType = new Type("int");
//        Place p1 = new Place(intType);
//        Place p2 = new Place(intType);
//        Transition t1 = new Transition();
//        Arc a1 = new ArcPtT(p1, t1);
//        Arc a2 = new ArcTtP(t1, p2);
//        try {
//            a1.setRule("x");
//            a2.setRule("x+y");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        p1.addToken(new Token(intType));
//        npn.addPlace(p1);
//        npn.addPlace(p2);
//        npn.addTransition(t1);
//        npn.addArc(a1);
//        npn.addArc(a2);
//        npn.toXML("testXMl.xml");
//        NestedPN npn2 = NestedPN.createInstance("testXMl.xml");
//        npn2.toXML("testDECODE.xml");

//        Cpn cpn = new Cpn();
//        List<Place> places = cpn.getPlaces();
//        Place p0 = new Place("p0", "INT", "3`1");
//        Place p1 = new Place("p1", "INT", "2`2++\n5`1");
//        places.add(p0);
//        places.add(p1);
//
//        List<Transition> transitions = cpn.getTransitions();
//        Transition t0 = new Transition("T0");
//        Transition t1 = new Transition("T1");
//        transitions.add(t0);
//        transitions.add(t1);
//
//
//        List<Arc> arcs = cpn.getArcs();
//        Arc a0 = new Arc(p0, t0, Arc.Orientation.PtoT, "x");
//        Arc a1 = new Arc(p1, t0, Arc.Orientation.TtoP, "x");
//        Arc a2 = new Arc(p1, t1, Arc.Orientation.PtoT, "x");
//        Arc a3 = new Arc(p0, t1, Arc.Orientation.TtoP, "x");
//        arcs.add(a0);
//        arcs.add(a1);
//        arcs.add(a2);
//        arcs.add(a3);
//
//        PrintWriter pw = new PrintWriter(new File("OUTPUT.cpn"));
//        pw.append(cpn.asXml());
//        pw.close();
//        System.out.print(cpn.asXml());
    }

}
