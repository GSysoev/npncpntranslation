package ru.hse.sg.nctranslation;

import org.omg.PortableInterceptor.ForwardRequest;
import ru.hse.pavel.model.*;
import ru.hse.pavel.model.pn.*;
import ru.hse.sg.nctranslation.models.cpn.Arc;
import ru.hse.sg.nctranslation.models.cpn.Transition;
import ru.hse.sg.nctranslation.models.cpn.*;
import ru.hse.sg.nctranslation.models.cpn.Place;
import sun.plugin.javascript.navig.Array;

import java.util.*;

/**
 * TODO: class description.
 * Date: 26.05.12
 * Time: 16:17
 *
 * @author German Sysoev (HSE)
 */
public class Translator {

    /* NPN to translate */
    private NestedPN npn;

    /* Result of the translation */
    private Cpn cpn;

    /* Linking between places */
    Map<Place, ru.hse.pavel.model.Place> linksC2N = new HashMap<Place, ru.hse.pavel.model.Place>();
    Map<ru.hse.pavel.model.Place, Place> linksN2C = new HashMap<ru.hse.pavel.model.Place, Place>();


    /**
     * Constructs a <tt>Translator</tt> object
     * @param npn NPN to translate, not modified
     */
    public Translator(NestedPN npn) {
        this.npn = npn;
        cpn = new Cpn();
        cpn.hashCode();
    }

    /**
     * Translates <tt>npn</tt> to <tt>Cpn</tt>.
     */
    public void translate() {
        definePlaces();
        defineEATrans();
        defineSimOfSATrans();
        //translateSynchTrans();
        defineMarking();
    }

    /* Defines places of CPN */
    private void definePlaces() {
        for (ru.hse.pavel.model.Place place: npn.getPlaces()) {
            if (!place.getType().isNet()) {
                Place place2 = new Place(place.getType().getName(), ""); //TODO
                linksC2N.put(place2, place);
                linksN2C.put(place, place2);
                cpn.getPlaces().add(place2);
            } else {
                Place place2 = new Place("LIST_INT", ""); //TODO
                linksC2N.put(place2, place);
                linksN2C.put(place, place2);
                cpn.getPlaces().add(place2);
            }
        }
    }


    /* Defines element-autonomous transitions in CPN */
    private void defineEATrans() {
        for (Place place: cpn.getPlaces()) {
            if (place.getType().equals("LIST_INT")) {
                PetriNet currPN = linksC2N.get(place).getType().getPetriNet();
                for (ru.hse.pavel.model.pn.Transition tran : currPN.getTrans()) {
                    int[] inVector = new int[currPN.getPlaces().size()];
                    int[] outVector = new int[currPN.getPlaces().size()];
                    int i = -1;
                    for (ru.hse.pavel.model.pn.Place place2 : currPN.getPlaces()) {
                        ++i;
                        for (ru.hse.pavel.model.pn.ArcPtT arcIn : tran.getArcsIn()) {
                            if (arcIn.getFrom() == place2) {
                                inVector[i] += arcIn.getRule();
                                outVector[i] -= arcIn.getRule();
                            }
                        }
                        for (ru.hse.pavel.model.pn.ArcTtP arcOut : tran.getArcsOut()) {
                            if (arcOut.getTo() == place2) {
                                outVector[i] += arcOut.getRule();
                            }
                        }
                    }
                    //TODO correct x and xr
                    String cond = "[ ms_le_el" + toCPNToolsStyle(inVector) + " x]";
                    String code =
                            "input(x);\n" +
                            "output(xr);\n" +
                            "action\n" +
                            "let\n" +
                            "  val rs = ms_add(x," + toCPNToolsStyle(outVector) + ");\n" +
                            "in\n" +
                            "  (rs)\n" +
                            "end;\n"
                            ;
                    Transition transition = new Transition(cond, code);
                    Arc arc1 = new Arc(place, transition, Arc.Orientation.PtoT, "x");
                    Arc arc2 = new Arc(place, transition, Arc.Orientation.TtoP, "xr");
                    cpn.getTransitions().add(transition);
                    cpn.getArcs().add(arc1);
                    cpn.getArcs().add(arc2);
                }
            }
        }
    }

    /* Defines simulation of system-autonomous transitions */
    private void defineSimOfSATrans() {
        for (ru.hse.pavel.model.Transition trans : npn.getTrans()) {
            if (trans.getSinc() == '\0') {
                Transition transition = new Transition();
                cpn.getTransitions().add(transition);

                for (ru.hse.pavel.model.Arc arc : trans.getArcsIn()) {
                    Arc arc1 = new Arc(
                            linksN2C.get((ru.hse.pavel.model.Place) (arc.getFrom())),
                            transition, Arc.Orientation.PtoT, arc.getRule());
                    cpn.getArcs().add(arc1);
                    cpn.getVars().add(arc.getRule());
                }

                for (ru.hse.pavel.model.Arc arc : trans.getArcsOut()) {
                    String rule = arc.getRule();
                    StringBuilder sb = new StringBuilder(rule);
                    for (int i = 0; i < sb.length(); ++i) {
                        if (sb.charAt(i) == '+') {
                            sb.setCharAt(i, '@');
                        }
                    }
                    rule = sb.toString();
                    String[] parts = rule.split("@");
                    for (int i = 0, n = parts.length; i < n; ++i) {
                        if (parts[i].charAt(0) > '9' || parts[i].charAt(0) < '0') {
                            parts[i] = "1`" + parts[i];
                        } else {
                            int j = 0;
                            while (Character.isDigit(parts[i].charAt(j++)));
                            parts[i] = parts[i].substring(0, i) + "`" + parts[i].substring(i, parts[i].length());
                        }
                    }
                    sb = new StringBuilder();
                    for (String s : parts) {
                        sb.append(s).append("++");
                    }
                    //TODO esli budet atomarnii to sb.length() = 0. ISPRAVIT
                    sb.deleteCharAt(sb.length() - 1);
                    sb.deleteCharAt(sb.length() - 1);

                    rule = sb.toString();

                    Arc arc1 = new Arc(
                            linksN2C.get((ru.hse.pavel.model.Place)(arc.getTo())),
                            transition, Arc.Orientation.TtoP, rule);
                    cpn.getArcs().add(arc1);
                }
            }
        }
    }

    /* Translates synchronized transitions*/
    private void translateSynchTrans() {
        for (ru.hse.pavel.model.Transition trans : npn.getTrans()) {
            if (trans.getSinc() != '\0') {
                Transition transition = new Transition();
                cpn.getTransitions().add(transition);

                for (ru.hse.pavel.model.Arc arc : trans.getArcsIn()) {
                    Arc arc1 = new Arc(
                            linksN2C.get((ru.hse.pavel.model.Place) (arc.getFrom())),
                            transition, Arc.Orientation.PtoT, arc.getRule());
                    cpn.getArcs().add(arc1);
                }

                for (ru.hse.pavel.model.Arc arc : trans.getArcsOut()) {
                    String rule = arc.getRule();
                    String[] parts = rule.split("+");
                    for (int i = 0, n = parts.length; i < n; ++i) {
                        if (parts[i].charAt(0) > '9' || parts[i].charAt(0) < '0') {
                            parts[i] = "1`" + parts[i];
                        } else {
                            int j = 0;
                            while (Character.isDigit(parts[i].charAt(j++)));
                            parts[i] = parts[i].substring(0, i) + "`" + parts[i].substring(i, parts[i].length());
                        }
                    }
                    rule.concat("++");

                    Arc arc1 = new Arc(
                            linksN2C.get((ru.hse.pavel.model.Place)(arc.getTo())),
                            transition, Arc.Orientation.TtoP, rule);
                    cpn.getArcs().add(arc1);
                }

                List<List<ru.hse.pavel.model.pn.Transition>> synhTrans =
                        new ArrayList<List<ru.hse.pavel.model.pn.Transition>>();

                for (ru.hse.pavel.model.Arc arc: trans.getArcsIn()) {
                    if (!((ru.hse.pavel.model.Place)arc.getFrom()).getType().isNet()) continue;
                    ArrayList<ru.hse.pavel.model.pn.Transition> currList =
                            new ArrayList<ru.hse.pavel.model.pn.Transition>();
                    synhTrans.add(currList);
                    PetriNet currPN = ((ru.hse.pavel.model.Place)arc.getFrom()).getType().getPetriNet();
                    for (ru.hse.pavel.model.pn.Transition tran : currPN.getTrans()) {
                        if (tran.getVerticalSinc() != trans.getSinc()) continue;
                        currList.add(tran);
//                        int[] inVector = new int[currPN.getPlaces().size()];
//                        int[] outVector = new int[currPN.getPlaces().size()];
//                        int i = -1;
//                        for (ru.hse.pavel.model.pn.Place place2 : currPN.getPlaces()) {
//                            ++i;
//                            for (ru.hse.pavel.model.pn.ArcPtT arcIn : tran.getArcsIn()) {
//                                if (arcIn.getFrom() == place2) {
//                                    inVector[i] += arcIn.getRule();
//                                    outVector[i] -= arcIn.getRule();
//                                }
//                            }
//                            for (ru.hse.pavel.model.pn.ArcTtP arcOut : tran.getArcsOut()) {
//                                if (arcOut.getTo() == place2) {
//                                    outVector[i] += arcOut.getRule();
//                                }
//                            }
//                        }
//                        //TODO correct x and xr
//                        String cond = "[ ms_le_el" + toCPNToolsStyle(inVector) + " x]";
//                        String code =
//                                "input(x);\n" +
//                                "output(xr);\n" +
//                                "action\n" +
//                                "let\n" +
//                                "  val rs = ms_add(x," + toCPNToolsStyle(outVector) + ");\n" +
//                                "in\n" +
//                                "  (rs)\n" +
//                                "end;\n"
//                                ;
//                        //Transition transition = new Transition(cond, code);
//                        transition.setCondition(cond);
//                        transition.setCode(code);
//
//                        Arc arc1 = new Arc(
//                                linksN2C.get((ru.hse.pavel.model.Place)arc.getFrom()),
//                                transition, Arc.Orientation.PtoT, "x");
//                        Arc arc2 = new Arc(
//                                linksN2C.get((ru.hse.pavel.model.Place)arc.getFrom()),
//                                transition, Arc.Orientation.TtoP, "xr");
//
//                        cpn.getTransitions().add(transition);
//                        cpn.getArcs().add(arc1);
//                        cpn.getArcs().add(arc2);
                    }
                    for (List<ru.hse.pavel.model.pn.Transition> list : synhTrans) {
                        for (ru.hse.pavel.model.pn.Transition tr : list) {
                                      //TODO SDELAT ETO SLOJNOE GAVNO
                        }
                    }
                }
            }
        }
    }

    /* Defines marking */
    private void defineMarking() {
        for (ru.hse.pavel.model.Place place : npn.getPlaces()) {
            if (!place.getType().isNet()) {
                linksN2C.get(place).setInitMark("1`()"); //TODO SDELAT NE 1
                continue;
            }
            if (place.getTokens().size() == 0) continue;

            StringBuilder mark = new StringBuilder("");
            for (Token token : place.getTokens()) {
                mark.append("1`" + toCPNToolsStyle(token.getMarking()) + "++\n");
            }
            mark.deleteCharAt(mark.length() - 1);
            mark.deleteCharAt(mark.length() - 1);
            mark.deleteCharAt(mark.length() - 1);
            linksN2C.get(place).setInitMark(mark.toString());
        }
    }

    /* Represents vector as string in CPNTools style*/
    private String toCPNToolsStyle(int[] arr) {
        StringBuilder res = new StringBuilder("[");
        for (int i = 0, n = arr.length; i < n; ++i) {
            res.append(
                    ((arr[i] < 0) ? ("~" + -arr[i]) : arr[i]) +
                            ",");
        }
        res.deleteCharAt(res.length() - 1);
        res.append("]");
        return res.toString();
    }

    public Cpn getCpn() {
        return cpn;
    }
}
