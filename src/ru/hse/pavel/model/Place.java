package ru.hse.pavel.model;

import java.util.ArrayList;
import java.util.List;

public class Place extends NPNObject {

	private Type type;
	
	private String name;

	private List<Token> tokens = new ArrayList<Token>();

	public Place(Type type) {
		
		super();
		this.type = type;

	}

	public void addToken(Token t) {
		if (t.getType() != type) {
			throw new IllegalArgumentException("addToken - type of the token differs from type of place");
		}
		tokens.add(t);
		System.out.println("token of type " + type.getName() + " added in place " + name + " id=" + id);
	}

	public Token getToken() {
		
		//TODO ������� ��������
		if (tokens.size() != 0) {
			return tokens.remove(tokens.size() - 1);
		}
		throw new IndexOutOfBoundsException("getToken() - there is no token to return");
		
	}

    public List<Token> getTokens() {
        return tokens;
    }

    public Type getType() {
		
		return type;
	
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

}
