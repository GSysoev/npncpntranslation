package ru.hse.pavel.model;

public abstract class NPNObject {
	
	static private int counter = 1;
	
	protected int id;
	
	public NPNObject() {
		id = counter++;
	}
	
	public int getId() {
		return id;
	}
	

}
