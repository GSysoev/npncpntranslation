package ru.hse.pavel.model;

public class Token {
	
	private Type type;
	
	private int[] marking;
	
	public Type getType() {
		return type;
	}

	public Token(Type type) {
		
		this.type = type;
		if (type.isNet()) {
			setMarking(type.getInitialMarking().clone());
		}
		
	}
	
	/**
	 * ��� ������������
	 * @param t
	 */
	private Token(Token t) {
		this.type = t.type;
		this.marking = t.getMarking().clone();
	}
	
	public Token clone() {
		return new Token(this);
	}

	public int[] getMarking() {
		return marking;
	}

	public void setMarking(int[] marking) {
		this.marking = marking;
	}
	
}
