package ru.hse.pavel.model;

public abstract class Arc extends NPNObject{
	
	/*
	 * rule, which determine how transition works
	 */
	protected String rule;
	
	protected NPNObject from;
	
	protected NPNObject to;
	
	protected Type type;
	
	public Arc() {}
	
	public Arc(NPNObject from, NPNObject to) {
		
		this.from = from;
		this.to = to;
	}
	

	
	/*
	 * changes the rule
	 */
	abstract public void setRule(String rule) throws RuleViolationException;
	
	/**
	 * rule getter
	 */
	public String getRule() {
		
		return rule;
		
	}

	public NPNObject getFrom() {
		return from;
	}

	public NPNObject getTo() {
		return to;
	}
	
	
}
