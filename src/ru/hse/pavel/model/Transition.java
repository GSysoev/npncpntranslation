package ru.hse.pavel.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * �������
 * @author asus
 *
 */
public class Transition extends NPNObject {

	Map<String,Token> tokensAr; //������, ������� �� ����� ��� ���������� �� �������� �������
	
	//List<String> rulesOut; //string ��?

    private char sinc = '\0';
	
	List<Arc> arcsIn;
	
	List<Arc> arcsOut;
	
	public Transition() {
		super();
		
		tokensAr = new HashMap<String,Token>();
		arcsIn = new LinkedList<Arc>();
		arcsOut = new LinkedList<Arc>();
		
	}
	
	/**
	 * ������������ ��������
	 */
	public void fire() {
		try {
			if (arcsIn.isEmpty() || arcsOut.isEmpty()) {
				throw new RuleViolationException(this.id);
			}
			takeTokens();
			for (Arc a : arcsOut) {
				Iterator<Map.Entry<String, Token>> itr = tokensAr.entrySet().iterator();
				while (itr.hasNext()) {
					
					Map.Entry<String,Token> temp = itr.next();
					
					((ArcTtP)a).putToken(temp.getKey(), temp.getValue());
				}
				    
			}
		}
		catch (RuleViolationException e) {
			//TODO ���� ���-�� ���������� �� ������� ����� ������ ������
		}
	}


	
	//����� ������ ��� �� ����������
	private void takeTokens() throws RuleViolationException {
		tokensAr.clear();
		
		for (Arc a : arcsIn) {
			if(tokensAr.containsKey(a.getRule())) {
				throw new RuleViolationException(this.id);
			}
			else {
				tokensAr.put(a.getRule(), ((Place)(a.from)).getToken());
			}
		}

	}
	
	//TODO ������� ����� isReady() ������ � ��������� ������)))
	
	public void addArc(ArcPtT arc){
		arcsIn.add(arc);
/*		if(ruleIn.containsKey(arc.getRule())) {
			throw new RuleViolationException(this.id);
		}
		else {
			ruleIn.put(arc.getRule(), ((Place)(arc.from)).getType());
		}*/
	}
	
	public void addArc(ArcTtP arc) {
		arcsOut.add(arc);
	}
	
	public void deleteArc(ArcPtT arc) {
		arcsIn.remove(arc);
		tokensAr.remove(arc.getRule());
	}
	
	public void deleteArc(ArcTtP arc) {
		arcsOut.remove(arc);
	}

    public char getSinc() {
        return sinc;
    }

    public void setSinc(char sinc) {
        this.sinc = sinc;
    }

    public List<Arc> getArcsIn() {
        return arcsIn;
    }

    public void setArcsIn(List<Arc> arcsIn) {
        this.arcsIn = arcsIn;
    }

    public List<Arc> getArcsOut() {
        return arcsOut;
    }

    public void setArcsOut(List<Arc> arcsOut) {
        this.arcsOut = arcsOut;
    }
}
