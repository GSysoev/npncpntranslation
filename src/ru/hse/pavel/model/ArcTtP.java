package ru.hse.pavel.model;

import java.util.HashMap;
import java.util.Map;

/**
 * ���� �� �������� � �������
 * @author asus
 *
 */
public class ArcTtP extends Arc {
	
	/**
	 * 3x- string - 'x', integer - 3
	 */
	private Map<String,Integer> rules; 

	@Override
	public void setRule(String rule) throws RuleViolationException {
		
		if (syntaxCheck(rule)) {
			this.rule = rule.replaceAll(" ", "");
			
			//���������� rules
			String[] ar = this.rule.split("\\+");
			for (String s : ar) {
				for (int i=0; i < s.length(); i++) {
					if (Character.isDigit(s.charAt(i))) {
						continue;
					}
					else {
						rules.put(s.substring(i), i == 0 ? 1 : Integer.parseInt(s.substring(0, i)));
					}
				}
			}
			
			
		} else {
			throw new RuleViolationException(this.id);
		}

	}

	private boolean syntaxCheck(String rule) {

		String[] ar = rule.split("\\+");
		for (String s : ar) {
			s = s.trim();
			int i = 0;
			for (; i < s.length(); i++) {
				if (Character.isDigit(s.charAt(i))) {
					continue;
				}
				if (Character.isLetter(s.charAt(i)))
					break;
				else
					return false;
			}
			if (i != s.length()) {
				for (; i < s.length(); i++) {
					if (Character.isLetter(s.charAt(i)))
						continue;
					else
						return false;
				}
			} else
				return false;
		}
		return true;
	}

	public ArcTtP(Transition from, Place to) {

		this.from = from;
		this.to = to;
		rules = new HashMap<String,Integer>();
		this.type = to.getType();
		from.addArc(this);

	}
	
	/**
	 * ��� ���������� var ��������� ����� t
	 * ����������� ���������� ��� � �������� �������
	 * @param var ���������� � ������� ������, 
	 * ������� �������� � ������������ �����
	 * @param t
	 */
	public void putToken(String var, Token t) {
		
		if (rules.containsKey(var)) {
			for (int i = rules.get(var); i != 0; i--) {
				((Place)to).addToken(t.clone());
			}
			return;
		}
		else return;
	
	}
	
	

}
