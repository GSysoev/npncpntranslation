package ru.hse.pavel.model;

/**
 * ���� �� ������� � ��������
 * @author asus
 *
 */
public class ArcPtT extends Arc {
	
	public ArcPtT(Place from, Transition to) {
		
		this.from = from;
		this.to = to;
		
		this.type = from.getType();
		to.addArc(this);
				
	}
	
	public void setRule(String rule) throws RuleViolationException {
		
		if (rule.isEmpty())  {
			return;
		}
		
		rule = rule.toLowerCase();
		rule = rule.trim();
		for (int i =0; i< rule.length(); i++) {
			if (rule.charAt(i) < 'a' || rule.charAt(i) > 'z') {
				throw new RuleViolationException(this.id);
			}
		}
		this.rule = rule;
		
	}
	//check
	
}
