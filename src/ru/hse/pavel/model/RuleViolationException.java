package ru.hse.pavel.model;

/**
 * ����������. � ��� ������� ����� ���������� � ����� ������� ��������� ������
 * @author asus
 *
 */
public class RuleViolationException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6643052998801939649L;
	public final int id;

	public RuleViolationException(int id) {
		super("Rule violation in NPNObject with ID: " + id);
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
}