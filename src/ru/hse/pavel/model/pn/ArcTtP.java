package ru.hse.pavel.model.pn;

import java.util.Map;

public class ArcTtP {
	/*
	 * �������, ������������ ������ ��������
	 */
	protected int rule;

	private Place to;
	private Transition from;
	/**
	 * 3x- string - 'x', integer - 3
	 */
	private Map<String, Integer> rules;

	public ArcTtP(Place to, Transition from) {
		this.setTo(to);
		this.setFrom(from);

		rule = 1;

		from.addArc(this);
	}

	//������� - �����
	public void setRule(String rule) {

		if (rule.isEmpty()) {
			return;
		}

		rule = rule.trim();
		try {
			int result = Integer.parseInt(rule);
			if (result >= 1) {
				this.rule = result;
			} else
				return;
		} catch (Exception e) {
			System.out.println("setRule(" + rule + ");");
		}

	}

	public int getRule() {
		return rule;
	}

	public boolean hasRule() {
		if (rule != 0) {
			return true;
		} else {
			return false;
		}
	}

	public Place getTo() {
		return to;
	}

	public void setTo(Place to) {
		this.to = to;
	}

	public Transition getFrom() {
		return from;
	}

	public void setFrom(Transition from) {
		this.from = from;
	}
}
