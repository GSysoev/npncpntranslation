package ru.hse.pavel.model.pn;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class Transition {

	private String name;
	
	private char verticalSinc = '\0';

	//TODO �������� ��� ������� ���� ������
	//Map<String,Token> tokensAr; //������, ������� �� ����� ��� ���������� �� �������� �������
	
	List<ArcPtT> arcsIn = new ArrayList<ArcPtT>();
	
	List<ArcTtP> arcsOut = new ArrayList<ArcTtP>();

    public List<ArcPtT> getArcsIn() {
        return arcsIn;
    }

    public void setArcsIn(List<ArcPtT> arcsIn) {
        this.arcsIn = arcsIn;
    }

    public List<ArcTtP> getArcsOut() {
        return arcsOut;
    }

    public void setArcsOut(List<ArcTtP> arcsOut) {
        this.arcsOut = arcsOut;
    }

    /**
	 * ������������ ��������
	 */
	public void fire() {
	
		if ( !isReady()) {
			return;
		}
		//��������� ������������ ���������� ����� �� �������
		for (ArcPtT arc : arcsIn) {
			arc.getFrom().setNumberOfTokens(arc.getFrom().getNumberOfTokens() - arc.getRule());
		}
		for (ArcTtP arc : arcsOut) {
			arc.getTo().setNumberOfTokens(arc.getTo().getNumberOfTokens()+arc.getRule());
		}
		//TODO ���� ��� �������������
	}
	
	public boolean isReady() {
		//TODO ���� ��� �������������
		if (arcsIn.isEmpty() || arcsOut.isEmpty()) {
			System.out.println("isReady - no arcs");
			return false;
		}
		else {
			for (ArcTtP arc : arcsOut) {
				if (!arc.hasRule()) {
					return false;
				}
			}
			//�������� ������� �������
			for (ArcPtT arc : arcsIn) {
				if (arc.hasRule()) {
					if (arc.getFrom().getNumberOfTokens() < arc.getRule()) {
						return false;
					}
				}
				else {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * ���� �� ������������ �������������
	 * ������������� ��������� ���� ������� ������ ���
	 * ��������� ������������ ��������
	 * @return
	 */
	public boolean isWaitingForSinc() {
		if (isReady()) {
			if (verticalSinc != ' ') {
				return true;
			}
		}
		return false;
	}
	
	/*
	 * ������� � �������
	 */
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getVerticalSinc() {
		return verticalSinc;
	}

	/**
	 * ������������� ������ ��� ������������ �������������
	 * @param verticalSinc
	 */
	public void setVerticalSinc(char verticalSinc) {
		if (Character.isLetter(verticalSinc)) {
		this.verticalSinc = verticalSinc;
		}
		throw new IllegalArgumentException("setVerticalSinc() - wrong argument");
	}
	
	/*
	 * ������ � ������
	 */
	
	public void addArc(ArcPtT arc){
		arcsIn.add(arc);
	}
	public void addArc(ArcTtP arc) {
		arcsOut.add(arc);
	}
	
	public void deleteArc(ArcPtT arc) {
		arcsIn.remove(arc);
		//���� ���-��
	}
	
	public void deleteArc(ArcTtP arc) {
		arcsOut.remove(arc);
	}
}
