package ru.hse.pavel.model.pn;

public class ArcPtT {
	/*
	 * �������, ������������ ������ ��������
	 */
	protected int rule;
	private Place from;
	private Transition to;

	public ArcPtT(Place from, Transition to) {

		this.setFrom(from);
		this.setTo(to);

		rule = 1;
		to.addArc(this);

	}

	//������� - �����
	public void setRule(String rule) {

		if (rule.isEmpty()) {
			return;
		}

		rule = rule.trim();
		try {
			int result = Integer.parseInt(rule);
			if (result >= 1) {
				this.rule = result;
			} else
				return;
		} catch (Exception e) {
			System.out.println("setRule(" + rule + ");");
		}

	}
	
	public int getRule() {
		return rule;
	}
	
	public boolean hasRule() {
		if (rule != 0) {
			return true;
		}
		else {
			return false;
		}
	}

	public Place getFrom() {
		return from;
	}

	public void setFrom(Place from) {
		this.from = from;
	}

	public Transition getTo() {
		return to;
	}

	public void setTo(Transition to) {
		this.to = to;
	}

}
