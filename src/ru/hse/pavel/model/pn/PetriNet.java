package ru.hse.pavel.model.pn;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class PetriNet {

	/*
	 * list of all places (not inner)
	 */
	List<Place> places = new LinkedList<Place>();
	
	/*
	 * list of all transitions (not inner)
	 */
	List<Transition> trans = new LinkedList<Transition>();
	
	//List<Arc> arcs = new LinkedList<Arc>(); 
	
	public PetriNet() {}
	
	/**
	 * ��������� ������� �������
	 * @param marking
	 */
	public void loadMarking(int[] marking) {
		if (marking.length != places.size()) {
			throw new IllegalArgumentException("loadMarking() - wrong argument");
		}
		for (int i = 0; i < marking.length; i++) {
			places.get(i).setNumberOfTokens(marking[i]);
		}
	}
	
	/**
	 * ��������� ���������� ���� � ������
	 * @return
	 */
	public int[] saveMarking() {
		int[] result = new int[places.size()];
		for (int i = 0; i < places.size(); i++) {
			result[i] = places.get(i).getNumberOfTokens();
		}
		return result;
	}
	
	public void addPlace(Place p) {
		places.add(p);
	}
	
	public void addTransition(Transition t) {
		trans.add(t);
	}
	
/*	public void addArc(Arc a) {
		arcs.add(a);
	}
	
	public void removeArc(int i) {
		arcs.remove(i);
	}*/
	
	public void removePlace(int i) {
		places.remove(i);
	}
	
	public void removeTransition(int i) {
		trans.remove(i);
	}

    public List<Place> getPlaces() {
        return places;
    }

    public List<Transition> getTrans() {
        return trans;
    }

    /*	public static void main(String[] args) {
         try {

             Type t = new Type("a");
             Place p1 = new Place(t);
             Transition tr = new Transition();
             ArcPtT arc = new ArcPtT(p1, tr);
             arc.setRule("A");
             ArcTtP arc1 = new ArcTtP(tr, p1);
             arc1.setRule("3a ");
             System.out.println(arc1.getRule());
             p1.addToken(new Token(t));
             tr.fire();
             tr.fire();


         }
         catch (Exception e) {
             System.err.println(e.getMessage());
         }
     }*/
	
}
