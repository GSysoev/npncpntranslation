package ru.hse.pavel.model.pn;

public class Place {
	
	//�������� �������
	private String name;

	//����� ����� � �������
	private int numberOfTokens;
	
	public Place() {
		name = "";
		numberOfTokens = 0;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumberOfTokens() {
		return numberOfTokens;
	}

	/**
	 * ���������� ����� �����
	 * @param numberOfTokens
	 */
	public void setNumberOfTokens(int numberOfTokens) {
		this.numberOfTokens = numberOfTokens;
	}
	
}
