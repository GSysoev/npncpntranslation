package ru.hse.pavel.model;

import ru.hse.pavel.model.pn.PetriNet;

/**
 * ��� ������� � �������
 * @author asus
 *
 */
public class Type {

	//�������� ������ ���� (������� ���� ������)
	private String Name;
	
	//�������� �� ��� ������� (����� ����� ���� �������)
	private boolean isNet;

	//��������� ����
	private PetriNet petriNet = null;
	
	//��������� ����������
	private int[] initialMarking;
	
	public Type(String type) throws IllegalArgumentException {
		
		if (!type.equals((String)(""))) {
			Name = type;
			isNet = false;
		}
		
		else {
			throw new IllegalArgumentException("Type() - empty string");
		}
		
	}
	
	/**
	 * ����������� ���� ��� ������� �����
	 * @param type
	 * @param isNet
	 * @throws IllegalArgumentException
	 */
	public Type(String type, boolean isNet) throws IllegalArgumentException {
		
		if (!type.equals((String)(""))) {
			Name = type;
			this.isNet = isNet;
		}
		
		else {
			throw new IllegalArgumentException("Type() - empty string");
		}
		
	}
	
	public String getName() {
		return Name;
	}

	/**
	 * �������� �� ��� �������
	 * @return
	 */
	public boolean isNet() {
		return isNet;
	}

	public void setNet(boolean isNet) {
		this.isNet = isNet;
	}

	public void setName(String name) {
		Name = name;
	}

	public int[] getInitialMarking() {
		return initialMarking;
	}

	public void setInitialMarking(int[] initialMarking) {
		this.initialMarking = initialMarking;
	}

    public PetriNet getPetriNet() {
        return petriNet;
    }

    public void setPetriNet(PetriNet petriNet) {
        this.petriNet = petriNet;
    }

    //TODO ����� ����� ������ ��� ��� setNet
	
/*	public boolean compareTo(Type t) {
		
		if (t.Name == Name) {
			return true;
		}
		
		return false;
		
	}*/
	
}
