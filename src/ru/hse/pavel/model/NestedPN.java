package ru.hse.pavel.model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.File;
import java.io.FileWriter;
import java.util.LinkedList;
import java.util.List;

public class NestedPN {
	
	/*
	 * list of all places (not inner)
	 */
	List<Place> places = new LinkedList<Place>();
	
	/*
	 * list of all transitions (not inner)
	 */
	List<Transition> trans = new LinkedList<Transition>();
	
	List<Arc> arcs = new LinkedList<Arc>();

    public List<Place> getPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }

    public List<Transition> getTrans() {
        return trans;
    }

    public void setTrans(List<Transition> trans) {
        this.trans = trans;
    }

    public List<Arc> getArcs() {
        return arcs;
    }

    public void setArcs(List<Arc> arcs) {
        this.arcs = arcs;
    }

    public List<Type> getTypes() {
        return types;
    }

    public void setTypes(List<Type> types) {
        this.types = types;
    }

    /**
     * Creates a <tt>NestedPN</tt> object from XML file
     * @param filename
     * @return
     */
    public static NestedPN createInstance(String filename) {
        XStream xstream = new XStream(new DomDriver());
        NestedPN pff = (NestedPN)xstream.fromXML(new File(filename));
        return pff;
    }
	
	public void addPlace(Place p) {
		places.add(p);
	}
	
	public void addTransition(Transition t) {
		trans.add(t);
	}
	
	public void addArc(Arc a) {
		arcs.add(a);
	}
	
	public void removeArc(int i) {
		arcs.remove(i);
	}
	
	public void removePlace(int i) {
		places.remove(i);
	}
	
	public void removeTransition(int i) {
		trans.remove(i);
	}
	
	List<Type> types = new LinkedList<Type>();
	
	public NestedPN() {}
	
	/**
	 * adds a type to a collection of types
	 * @param type
	 * @throws IllegalArgumentException
	 */
	public void addType(String type)
			throws IllegalArgumentException {
		
		for (Type t : types) {
			if (type == t.getName())
				throw new IllegalArgumentException("addType(" + type + ") - this type already exists");
		}
		
		types.add(new Type(type));
		
	}

    /**
     * Serialize <tt>NestedPN</tt> as XML
     *
     * @param filename name of the output XML file
     */
    public void toXML(String filename) {
        try {
            FileWriter out = new FileWriter(filename);
            XStream xstream = new XStream(new DomDriver());
            String xml = xstream.toXML(this);
            out.append(xml);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	public static void main(String[] args) {
		try {
			
			Type t = new Type("a");
			Place p1 = new Place(t);
			Transition tr = new Transition();
			ArcPtT arc = new ArcPtT(p1, tr);
			arc.setRule("A");
			ArcTtP arc1 = new ArcTtP(tr, p1);
			arc1.setRule("3a ");
			System.out.println(arc1.getRule());
			p1.addToken(new Token(t));
			tr.fire();
			tr.fire();
			
			
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
}
